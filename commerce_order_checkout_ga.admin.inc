<?php
/**
 * @file admiral-checkout-ga.admin.inc
 */

/**
 *  Implements hook_form().
 */
function commerce_order_checkout_ga_form($form, &$form_state)
{
  $form['contact_information'] = array(
    '#markup' => 'Settings form for the Commerce Checkout Google Analytics Module',
  );

  $line_item_types = commerce_line_item_types();

  $form['line_items'] = array(
    '#type' => 'container',
    '#tree' => true,
    '#attributes' => array(
      'class' => array(
        'line-item-container',
      ),
    ),
    '#weight' => 10,
  );

  $selected_line_items = variable_get('commerce_order_checkout_ga_line_items', '');

  if ($selected_line_items != '') {
    $selected_line_items = explode(',', $selected_line_items);
  }

  // var_dump($selected_line_items);

  foreach ($line_item_types as $key => $item) {
    if ($item['product'] == true) {
      //dsm($key);
      $form['line_items'][$key] = array(
        '#type' => 'checkbox',
        '#title' => $key,
        '#default_value' => (is_array($selected_line_items) && in_array($key, $selected_line_items) ? true : false),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#name' => 'submit',
    '#value' => t('Submit'));

  return $form;

}


function commerce_order_checkout_ga_form_submit($form, &$form_state) {
  //dsm($form_state);
  $selected_line_items  = array();
  $line_items = $form_state['values']['line_items'];
  foreach($line_items as $key => $value) {
    //dsm($value);
    if ($value == true) {
      $selected_line_items[] = $key;
    }
  }
  $selected_line_items = implode(',', $selected_line_items);
  variable_set('commerce_order_checkout_ga_line_items', $selected_line_items);
}
